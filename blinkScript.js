
  
    // Get the div element by its ID 
    const blinkDiv = 
        document.getElementById('blinkDiv'); 

    // An array of colors to be used for blinking 
    const colors = [ '#f55700', '#f57700',  '#f5a100']; 
    console.log(colors.length)
    // Variable to keep track of the 
    // current color index 
    let currentColorIndex = 0; 

    // Function to toggle the background 
    // color of the div 
    function blinkBackground() { 
        blinkDiv.style.backgroundColor = 
            colors[currentColorIndex]; 
        currentColorIndex = 
            (currentColorIndex + 1) % colors.length;

    } 

    // Start the blinking by setting an interval  
    // that calls the blinkBackground  
    // function every 1000ms (1 second) 
    const blinkingInterval = 
        setInterval(blinkBackground, 1000); 

    // To stop blinking after 5 seconds,  
    // use setTimeout to clear the interval 
    setTimeout(() => { 
        setInterval(blinkingInterval); 
    }, 5000); 
